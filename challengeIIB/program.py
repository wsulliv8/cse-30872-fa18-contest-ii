#!/usr/bin/env python3

import sys
import math

def read_matrix(rows):
    matrix = list(list())

    for _ in range(rows):
        row = list(map(int, sys.stdin.readline().split()))
        matrix.append(row)

    return matrix

def make_table(matrix, rows, cols):
    table = [[[0,0,0] for _ in range(cols)] for _ in range(rows)]

    for col in range(cols):
        for row in range(rows):
            if col == 0:
                table[row][col] = [matrix[row][col], row, 0]
            else:
                num1 = table[row][col-1][0]

                if row == 0:
                    if rows > 2:
                        num2 = table[rows-1][col-1][0]
                    else:
                        num2 = math.inf
                else:
                    num2 = table[row-1][col-1][0]

                if row == rows-1:
                    num3 = table[0][col-1][0]
                else:
                    num3 = table[row+1][col-1][0]

                if num2 <= num3 and num2 <= num1:
                    if row == 0:
                        table[row][col] = [matrix[row][col] + num2, rows-1, col-1]
                    else:
                        table[row][col] = [matrix[row][col] + num2, row-1, col-1]
                elif num1 <= num2 and num1 <= num3:
                    table[row][col] = [matrix[row][col] + num1, row, col-1]
                else:
                    if row == rows-1:
                        table[row][col] = [matrix[row][col] + num3, 0, col-1]
                    else:
                        table[row][col] = [matrix[row][col] + num3, row+1, col-1]

    return table

def shortest_path(table, rows, cols):
    min_cost = math.inf
    min_row = math.inf
    col = cols-1

    for row in range(rows):
        if table[row][col][0] < min_cost:
            min_cost = table[row][col][0]
            min_row = row

    path = []
    row = min_row

    while col != 0:
        path.append(row+1)
        row = table[row][col][1]
        col = table[row][col][2]
    path.append(row+1)

    return path[::-1], min_cost

if __name__ == '__main__':
    while True:
        try:
            line = sys.stdin.readline().split()
            rows = int(line[0])
            cols = int(line[1])
        except ValueError:
            break
        except IndexError:
            break

        matrix = read_matrix(rows)
        table = make_table(matrix, rows, cols)
        path, min_cost = shortest_path(table, rows, cols)

        print(min_cost)
        print(' '.join(map(str,path)))
