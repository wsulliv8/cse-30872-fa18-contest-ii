#!/usr/bin/env python3

import sys



if __name__ == '__main__':
   
    while True:
        try:
            a = list(input().strip())
        except EOFError:
            break
        b = list(input().strip())
        if len(a) <= len(b):
            smaller = a
            larger =b 
        else:
            smaller =  b
            larger = a
        numC = len(smaller)
        sub = []
        for i in range(numC):
            if smaller[i] in larger:
                larger.remove(smaller[i])
                sub.append(smaller[i])
        print(''.join(sorted(sub)))
            
        
