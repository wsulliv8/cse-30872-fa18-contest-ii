#!/usr/bin/env python3

import sys
import collections


def read_graph(nodes,edges):
    graph = collections.defaultdict(list)
    for node in range(1,nodes+1):
        graph[node] = []
    for _ in range(edges):
        line = input().strip().split()
        fr, to = map(int, line)
        graph[to].append(fr)
        graph[fr].append(to)
    
    return graph

def DFS(g, v, marked):
    if v in marked:
        return
    
    marked.append(v)
    for u in g[v]:
        DFS(g,u,marked)

    return marked
    

if __name__ == '__main__':
    counter = 1
    while True:
        try:
            numNodes = int(input().strip())
        except EOFError:
            break
        numEdges = int(input().strip())
        graph = read_graph(numNodes,numEdges)
        groups = []
        while len(graph) != 0:
            group = DFS(graph,list(graph.keys())[0],[])
            groups.append(group)
            for node in group:
                graph.pop(node)
        if len(groups) > 1:
            print('Graph {} has {} groups:'.format(counter,len(groups)))
        elif len(groups) == 1:
            print('Graph {} has {} group:'.format(counter,len(groups)))
        else:
            print('Graph {} has {} group:'.format(counter,0))
        for group in groups:
            group = sorted(group)
            print(' '.join(map(str,group)))
        counter += 1
            
        
