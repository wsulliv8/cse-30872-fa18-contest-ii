#!/usr/bin/env python3

import sys
import collections

saved_hops = collections.defaultdict(int)

def hip_hop(diff):
    if saved_hops[diff]:
        return saved_hops[diff]
    stay_time = 1
    hops = 1
    i = 1
    stays = 0
    reached_num = False
    while i <= diff:
        j = 0

        while j < stay_time:
            
            saved_hops[i+j] = hops
            if j + i == diff:
                reached_num = True
                break
            j += 1

        if reached_num:
            break
        
        i += stay_time

        if stays % 2:
            stay_time += 1
        hops += 1
        stays += 1
    return hops

if __name__ == '__main__':
    for line in sys.stdin:
        line = line.strip()
        nums = list(map(int, line.split()))
        num1 = nums[0]
        num2 = nums[1]
        diff = abs(num2 - num1)
        if saved_hops[diff]:
            if saved_hops[diff] == 1:
                print('{} -> {} takes {} hops'.format(num1, num2, saved_hops[diff]))
            else:
                print('{} -> {} takes {} hops'.format(num1, num2, saved_hops[diff]))
            continue
        if diff == 0:
            print('{} -> {} takes {} hops'.format(num1, num2, 0))
            continue
        hops = hip_hop(diff)
        if hops != 1:
            print('{} -> {} takes {} hops'.format(num1, num2, hops))
        else:
            print('{} -> {} takes {} hop'.format(num1, num2, hops))

