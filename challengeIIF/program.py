#!/usr/bin/env python3

import sys


if __name__ == '__main__':
    while True:
        try:
            numTacos = int(input().strip())
        except EOFError:
            break
        tacos = sorted(map(int,input().strip().split()), reverse=True)
        miles = 0
        for i in range(len(tacos)):
            miles += 2**i * tacos[i]
        print(miles)
