#!/usr/bin/env python3

import sys
import collections

def make_graph(dictionary):
    graph = collections.defaultdict(list)
    for word in dictionary:
        for i in range(dictionary.index(word), len(dictionary)):
            word2 = dictionary[i]
            count = 0
            is_morph = False
            if len(word) == len(word2):
                for j in range(len(word)):
                    if word[j] != word2[j]:
                        count += 1
                if count == 1:
                    is_morph = True
            elif len(word) == len(word2) + 1:
                for k in range(len(word2)):
                    if word2[k] not in word:
                        count += 1
                if count == 0:
                    is_morph = True
            elif len(word2) == len(word) + 1:
                for l in range(len(word)):
                    if word[l] not in word2:
                        count += 1
                if count == 0:
                    is_morph = True
            if is_morph:
                graph[word].insert(0, word2)
    return graph

def word_morph_seq(word, graph):
    frontier = [(word,word)]
    visited = collections.defaultdict(str)

    while frontier:
        src, dst = frontier.pop()

        if dst in visited:
            continue

        visited[dst] = src

        for neighbor in graph[dst]:
            frontier.append((dst, neighbor))

    return visited

def reconstruct_path(visited, dst):
    curr = dst
    seen = set()
    path = []
    
    while curr not in seen:
        path.append(curr)
        seen.add(curr)
        curr = visited[curr]

    return path[::-1]


if __name__ == '__main__':
    dictionary = []
    possibilities = []
    for line in sys.stdin:
        word = line.strip()
        dictionary.append(word)

    dictionary = sorted(dictionary)
    #print(dictionary)
    graph = make_graph(dictionary)
    #print(graph)
    path_max = []

    for word in dictionary:
        visited = word_morph_seq(word, graph)
        for dst in list(visited.keys()):
            path = reconstruct_path(visited, dst)
            if len(path) > len(path_max):
                path_max = path

    print(len(path_max))
    print('\n'.join(path_max))








